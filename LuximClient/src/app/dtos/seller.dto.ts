import { orderStatus } from '../shared/enums/order-status.enum';

export interface SellerDto {
  sellerId: string;
  sellerName: string;
  totalOrdersAmount: number;
  totalLuximCommissions: number;
  totalProcessingFees: number;
  totalAmountsReceived: number;
  totalAmountsPending: number;
}

export interface SellerDetailDto {
  id: string;
  name: string;
  userName: string;
  email: string;
  bankDetails: SellerBankDetails;
  orders: SellerOrderDetailDto[];
}

export interface SellerOrderDetailDto {
  orderId: number;
  orderNumber: string;
  orderAmount: number;
  luximCommission: number;
  luximProcessingFee: number;
  amountToBePaid: number;
  payoutStatus: number;
  orderStatus: orderStatus;
  payoutStatusText: string;
  shipmentStatus: string;
  shipmentStatusText: string;
  trackingId: string;
}

export interface SellerBankDetails {
  bankName: string;
  accountTitle: string;
  accountNumber: string;
}
