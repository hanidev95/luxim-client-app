export interface OrderHistoryDto {
  orderId: number;
  orderNumber: string;
  orderDate: Date;
  itemsQuantity: number;
  sellerUserName: string;
  sellerId: string;
  buyerUserName: string;
  buyerId: string;
  orderAmount: number;
  shippingStatus: string;
}
