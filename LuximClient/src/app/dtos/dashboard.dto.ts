export interface DashboardWidgetDto {
  widgetOrderDto: DashboardWidgetOrderDto;
  widgetUserDto: DashboardWidgetUserDto;
  widgetProductDto: DashboardWidgetProductDto;
}

export interface DashboardWidgetOrderDto {
  totalOrders: number;
  totalShippedOrders: number;
  totalConfirmedOrders: number;
  totalUnshippedOrders: number;
}

export interface DashboardWidgetUserDto {
  totalUsers: number;
  totalActiveUsers: number;
  inActiveUsers: number;
}

export interface DashboardWidgetProductDto {
  totalProducts: number;
  totalApprovedProducts: number;
  totalRejectedProducts: number;
  totalPendingProducts: number;
}
