import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { reportedUserModel } from '../../models/usermodel';
import { DashboardService } from '../../services/dashboard.service';
/*import { NotificationService } from '../../shared/notification.service';*/
@Component({
  selector: 'app-reported-users',
  templateUrl: './reported-users.component.html',
  styleUrls: ['./reported-users.component.scss']
})
export class ReportedUsersComponent implements OnInit {


  constructor(private service: DashboardService, private fb: FormBuilder, private toastr: ToastrService  /*private notifyService: NotificationService*/) { }
  reportedUsers: reportedUserModel[];
  reportedUserForm = this.fb.group({
    userId: [''],
    status: [0]
  });

  pageOfItems: Array<any>;
  ngOnInit(): void {
    this.getReportedUsers();
  }

  getReportedUsers() {
    //debugger;
    this.service.getReportedUsers().subscribe((response) => {

      this.reportedUsers = response.data;
    }, error => {

      console.log(error.error);

    });
  }

  manageReportedusers(id: any, status) {
  /*  this.notifyService.showSuccess("Data shown successfully !!", "tutsmake.com");*/
    this.reportedUserForm.controls["userId"].setValue(id);
    this.reportedUserForm.controls["status"].setValue(status);
    this.service.manageReportedUsers(this.reportedUserForm.getRawValue()).subscribe((response) => {

      if (response.succeeded) {
        this.toastr.success('Operation performed successfully');
        this.getReportedUsers();
      }
    },
      err => {
        this.toastr.error('Something went wrong', 'Reported Users');
        console.log(err.error);
      }

    )
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
}
