import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubcategoriesComponent } from './subcategories/subcategories.component';
import { JwPaginationModule } from 'jw-angular-pagination';

@NgModule({
  declarations: [CategoriesComponent, SubcategoriesComponent],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    JwPaginationModule,
  ],
})
export class CategoriesModule {}
