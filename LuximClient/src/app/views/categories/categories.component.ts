import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { categoryModel, subCategorymodel } from '../../models/categorymodel';
import { CategoryService } from '../../services/category.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  //subCategoryForm = this.fb.group({
  //  id: [0],
  //  categoryId: [0],
  //  name: ['', Validators.required],
  //  description: ['', Validators.required],
  //  subCategoryReference: [''],
  //  isActive: []


  //});
  //selectedType;
  //display = "none";
  categories: categoryModel[];
  subCategories: subCategorymodel[];
  isSubCategoryExists: boolean;
    pageOfItems: any[];
    items: { id: number; name: string; }[];
/*  dtOptions: DataTables.Settings = {};*/
  constructor(private service: CategoryService, private fb: FormBuilder, private bsModalService: BsModalService, private toastr: ToastrService) {  }

  ngOnInit(): void {
    
    //this.dtOptions = {
    //  pagingType: 'full_numbers',
    //  pageLength: 5,
    //  processing: true
    //};
    this.getAllCategories();

    this.items = Array(150).fill(0).map((x, i) => ({ id: (i + 1), name: `Item ${i + 1}` }));
  }

 

  getAllCategories() {

    return this.service.getAllCategories().subscribe(
      response => { this.categories = response.data },
      error => {
        console.log(error);
      })
  }

  // change pagination
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  //getAllSubCategories() {

  //  return this.service.getAllSubCategories().subscribe(
  //    response => { this.subCategories = response.data},
  //    error => {
  //      console.log(error);
  //    }    )
  //}



  //onChange(event) {
  //  this.selectedType = event.target.options[event.target.options.selectedIndex].text;
  //}


  

  //manageSubCategory() {

  // // this.onCloseHandled()
  //  this.service.manageSubCategories(this.subCategoryForm.value).subscribe(
  //    response => {

  //      if (response.succeeded) {
  //        this.getAllSubCategories();
  //        this.onCloseHandled()
  //      }
  //    },
  //    error => {
  //      console.log(error);
  //    });
  //}


  //checkIsSubCategoryAlreadyExits() {
  //  if (this.subCategoryForm.get('name').value.length >= 3) {

  //    this.service.checkIfSubCategoryExists(this.subCategoryForm.get('name').value, this.subCategoryForm.get('id').value).subscribe(() => { this.isSubCategoryExists = false; }, error => {

  //      this.isSubCategoryExists = true;
  //    })
  //  }
    
  //}



  //openModal(id: number) {
  //  this.getAllCategories();

  //  if (id > 0) {

  //    this.service.getSubCategoryById(id).subscribe(response => {

  //      if (response.succeeded) {

  //        this.subCategoryForm.controls["categoryId"].setValue(response.data.category == 'Product' ? 1 : response.data.categoryId);
  //        this.subCategoryForm.controls["name"].setValue(response.data.name);
  //        this.subCategoryForm.controls["description"].setValue(response.data.description);
  //        this.subCategoryForm.controls["isActive"].setValue(response.data.isActive);
  //        this.subCategoryForm.controls["subCategoryReference"].setValue(response.data.subCategoryReference);
  //        this.subCategoryForm.controls["id"].setValue(response.data.id);
  //        //this.subCategoryForm.patchValue({
  //        //  id: id,
  //        //  categoryId: response.data.categoryId,
  //        //  name: response.data.name,
  //        //  description: response.data.description,
  //        //  isActive: response.data.isActive,
  //        //  subCategoryReference: response.data.subCategoryReference
  //        //});
  //        this.selectedType = response.data.category;

         
  //      }

  //    },
  //      error => { })
  //  }
  //  else {

  //    this.subCategoryForm.controls["categoryId"].setValue(0);
  //    this.subCategoryForm.controls["name"].setValue("");
  //    this.subCategoryForm.controls["description"].setValue("");
  //    this.subCategoryForm.controls["isActive"].setValue(false);
  //    this.subCategoryForm.controls["subCategoryReference"].setValue("");
  //    this.subCategoryForm.controls["id"].setValue(0);
  //    this.selectedType = '';
  //  }
    
  //    this.display = "block";
  //  }
  //onCloseHandled() {
  //  this.isSubCategoryExists = false;
  //  this.selectedType = '';
  //    this.display = "none";
  //  }

   
  }

