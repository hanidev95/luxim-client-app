import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { categoryModel, subCategorymodel } from '../../../models/categorymodel';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.component.html',
  styleUrls: ['./subcategories.component.scss']
})
export class SubcategoriesComponent implements OnInit {

  subCategoryForm = this.fb.group({
    id: [0],
    categoryId: [0],
    name: ['', Validators.required],
    description: [''],
    subCategoryReference: [''],
    isActive: []


  });
  selectedType;
  display = 'none';
  categories: categoryModel[];
  subCategories: subCategorymodel[];
  items: [];
  isSubCategoryExists: boolean;
  pageOfItems: any[];
  isUpdate: boolean;
  constructor(private service: CategoryService, private fb: FormBuilder, private bsModalService: BsModalService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAllSubCategories();
  }



  getAllCategories() {

    return this.service.getAllCategories().subscribe(
      response => { this.categories = response.data;  },
      error => {
        console.log(error);
      });
  }

  getAllSubCategories() {

    return this.service.getAllSubCategories().subscribe(
      response => { this.subCategories = response.data; this.items = response.data; },
      error => {
        console.log(error);
      });
  }



  onChange(event) {
    this.selectedType = event.target.options[event.target.options.selectedIndex].text;
  }




  manageSubCategory() {
    // this.onCloseHandled()
    this.service.manageSubCategories(this.subCategoryForm.value).subscribe(
      response => {

        if (response.succeeded) {

          this.toastr.success('Operation performed successfully');
          this.getAllSubCategories();
          this.onCloseHandled();
        }
      },
      error => {
        this.toastr.error('Something went wrong');
        console.log(error);
      });
  }


  checkIsSubCategoryAlreadyExits() {
    const subCategoryTitle = this.subCategoryForm.get('name').value;
    if (!subCategoryTitle) {
      return;
    }
    const categoryId = this.subCategoryForm.get('categoryId').value;
    const subCategoryReference = this.subCategoryForm.get('subCategoryReference').value;

    const request = {
      subCategoryTitle : subCategoryTitle,
      categoryId : categoryId,
      subCategoryReference : subCategoryReference,
    };

    this.service.checkIfSubCategoryExists(request).subscribe(
      (resp) => {
        this.isSubCategoryExists = resp.data;
      }, error => {
        this.isSubCategoryExists = true;
    });
  }



  openModal(id: number) {
    this.getAllCategories();

    if (id > 0) {
      this.isUpdate = true;
      this.service.getSubCategoryById(id).subscribe(response => {

        if (response.succeeded) {

          this.subCategoryForm.controls['categoryId'].setValue(response.data.category === 'Product' ? '1' : response.data.categoryId);
          this.subCategoryForm.controls['name'].setValue(response.data.name);
          this.subCategoryForm.controls['description'].setValue(response.data.description);
          this.subCategoryForm.controls['isActive'].setValue(response.data.isActive);
          this.subCategoryForm.controls['subCategoryReference'].setValue(response.data.subCategoryReference);
          this.subCategoryForm.controls['id'].setValue(response.data.id);
          this.selectedType = response.data.category;
        }

      },
        error => { });
    } else {
      this.isUpdate = false;
      this.subCategoryForm.controls['categoryId'].setValue(0);
      this.subCategoryForm.controls['name'].setValue('');
      this.subCategoryForm.controls['description'].setValue('');
      this.subCategoryForm.controls['isActive'].setValue(false);
      this.subCategoryForm.controls['subCategoryReference'].setValue('');
      this.subCategoryForm.controls['id'].setValue(0);
      this.selectedType = '';
    }

    this.display = 'block';
  }
  onCloseHandled() {
    this.isSubCategoryExists = false;
    this.selectedType = '';
    this.display = 'none';
  }

   // change pagination
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

}
