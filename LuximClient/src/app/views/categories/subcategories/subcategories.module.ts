import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubcategoriesComponent } from './subcategories.component';
import { JwPaginationModule } from 'jw-angular-pagination';



@NgModule({
  declarations: [SubcategoriesComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JwPaginationModule
  ]
})
export class SubcategoriesModule { }
