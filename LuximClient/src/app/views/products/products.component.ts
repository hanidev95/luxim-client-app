import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { productImagesModel, productmodel, productRejectionReasons } from '../../models/product.model'
import { error } from '@angular/compiler/src/util';
import { BsModalService,BsModalRef } from 'ngx-bootstrap/modal';
import { ApproveRejectComponent } from './approve-reject/approve-reject.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: productmodel[];
  productImages : productImagesModel[];
  public bsModalRef: BsModalRef;
    pageOfItems: any[];

  constructor(private service: ProductService, private bsModalService: BsModalService) { }

  ngOnInit(): void {

    this.getAllProducts();
  }

  getAllProducts() {
    this.service.getAllProducts().subscribe(
      response => {
        this.products = response.data;
      },
      error => {
        console.log(error);
      }

    )
  }

  getProductImages(productId : number) {
    this.service.getproductImages(productId).subscribe(
      response => {
        this.bsModalRef.content.productImages = response.data;
      },
      error => {
        console.log(error);
      }
    )
  }

  showPopup(id: number, approvalStatus: string, rejectionReasonId : any) {
  
    this.bsModalRef = this.bsModalService.show(ApproveRejectComponent);
    this.getProductImages(id);
    this.bsModalRef.content.productApprovalForm.controls.productId.setValue(id);
    
    let status = (approvalStatus == 'Pending' || approvalStatus == 'Rejected' ? 1 : 2);
   /* this.bsModalRef.content.productApprovalForm.controls.approvalStatus = status;*/
    this.bsModalRef.content.selectedType = status;
    // this.bsModalRef.content.productImages = this.getProductImages(id);
    this.bsModalRef.content.productApprovalForm.controls['approvalStatus'].setValue(status);
    this.bsModalRef.content.productApprovalForm.controls['rejectionReasonId'].setValue(rejectionReasonId);

    
    this.bsModalRef.onHide.subscribe(result => {

      //console.log(result);
      //if (result == 'backdrop-click')
      //{
        this.getAllProducts();
      }
      
    );
  }

  // change pagination
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

 

}
