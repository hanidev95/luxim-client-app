import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormControl, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import {
  productImagesModel,
  productRejectionReasons,
} from '../../../models/product.model';
import { ProductService } from '../../../services/product.service';
/*import { NotificationService } from '../../../shared/notification.service';*/
@Component({
  selector: 'app-approve-reject',
  templateUrl: './approve-reject.component.html',
  styleUrls: ['./approve-reject.component.scss'],
})
export class ApproveRejectComponent implements OnInit {
  selectedType = '1';
  productImages: productImagesModel[];
  rejectionReasons: any[];
  productApprovalForm = this.fb.group({
    productId: [],
    approvalStatus: [0],
    // rejectionReason: ['', Validators.required],
    // rejectionReasonId : [0, Validators.required],
    rejectionReasonId: new FormControl(0, [Validators.required]),
  });

  constructor(
    private service: ProductService,
    private route: Router,
    private fb: FormBuilder,
    public activeModal: BsModalRef,
    private toastr: ToastrService
  ) {}
  get rejectionReasonId() {
    return this.productApprovalForm.get('rejectionReasonId');
  }
  onChange(event) {
    this.selectedType = event.target.value;
  }

  ngOnInit(): void {
    this.getRejectionReasons();
  }

  onSubmit() {
    this.service
      .manageProductApproval(this.productApprovalForm.getRawValue())
      .subscribe(
        (response: any) => {
          if (response.succeeded) {
            this.toastr.success('Operation performed successfully');
            this.activeModal.hide();
            /* this.notifyService.showSuccess("Action has been performed successfully", "Luxim Admin")*/
          }
        },
        (err) => {
          if (!err.error.Succeeded) {
            this.toastr.error('Something went wrong');
          } else {
            alert('sss');
          }
        }
      );
  }

  getProductImages(productId: number) {
    this.service.getproductImages(productId).subscribe(
      (response) => {
        this.productImages = response.data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getRejectionReasons() {
    this.service.getproductRejectionReasons().subscribe(
      (response) => {
        this.rejectionReasons = response.data;
      },
      (error) => {
        this.toastr.error('Rejection reasons not found');
      }
    );
  }
}
