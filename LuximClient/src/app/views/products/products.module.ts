import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ApproveRejectComponent } from './approve-reject/approve-reject.component';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwPaginationModule } from 'jw-angular-pagination';


@NgModule({
  declarations: [
    ProductsComponent,
    ApproveRejectComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    JwPaginationModule
  ],
  providers: [BsModalRef]
})
export class ProductsModule { }
