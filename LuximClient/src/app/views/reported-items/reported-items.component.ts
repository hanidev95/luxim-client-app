import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { reportedProductModel } from '../../models/product.model';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-reported-items',
  templateUrl: './reported-items.component.html',
  styleUrls: ['./reported-items.component.scss']
})
export class ReportedItemsComponent implements OnInit {
  pageOfItems: any[];

  constructor(private service: ProductService, private fb: FormBuilder, private toastr: ToastrService) { }
  reportedItems: reportedProductModel[];
  reportedItemForm = this.fb.group({
    productId: [0],
    status: [0]
  });
  ngOnInit(): void {
    this.getReportedItems();
  }

  getReportedItems() {
    
    this.service.getReportedProducts().subscribe((response) => {

      this.reportedItems = response.data;
    }, error => {

      console.log(error.error);

    });
  }

  manageReportedItems(id: any, status) {
    this.reportedItemForm.controls["productId"].setValue(id);
    this.reportedItemForm.controls["status"].setValue(status);
    this.service.manageReportedProducts(this.reportedItemForm.getRawValue()).subscribe((response) => {

      if (response.succeeded) {
        this.toastr.success('Operation performed successfully');
        this.getReportedItems();
      }
    },
      err => {
        this.toastr.error('Something went wrong', 'Reported Items');
        console.log(err.error);
      }

       )
  }

  //Pagination change event
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
}
