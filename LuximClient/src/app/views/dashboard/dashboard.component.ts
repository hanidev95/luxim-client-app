import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DashboardWidgetDto } from '../../dtos/dashboard.dto';
import { DataService } from '../../services/seller.service';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  widgetsData: DashboardWidgetDto;

  constructor(
    private dataService: DataService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.dataService.getWidgetData().subscribe(
      (response) => {
        this.widgetsData = response.data;
      },
      (err) => {
        this.toastr.error('Unable to retrieve dashboard data.', 'Dashboard');
      }
    );
  }
}
