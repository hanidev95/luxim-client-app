import { DecimalPipe, DatePipe } from '@angular/common';
import { Component, OnInit, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { OrderHistoryDto } from '../../../dtos/orders.dto';
import { DataService } from '../../../services/seller.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss'],
  providers: [DecimalPipe, DatePipe],
})
export class OrderHistoryComponent implements OnInit {
  orders: OrderHistoryDto[];
  filteredOrders: OrderHistoryDto[];
  searchCtrl = new FormControl('');

  constructor(
    private dataService: DataService,
    private toastr: ToastrService,
    private decimalPipe: DecimalPipe,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.getOrderHistory();
  }

  getOrderHistory() {
    this.dataService.getAllOrders().subscribe(
      (response) => {
        this.orders = this.filteredOrders = response.data;
      },
      (error) => {
        this.toastr.error('Something went wrong', 'Orders History');
      }
    );
  }

  onSearchEvent = (event: any): void => {
    this.search(event.target.value);
  };

  search = (text: string): void => {
    this.filteredOrders = this.orders.filter((order) => {
      const term = text.toLowerCase();
      console.log(this.datePipe.transform(order.orderDate));
      return (
        order.orderId.toString().toLowerCase().includes(term) ||
        order.orderNumber.toString().toLowerCase().includes(term) ||
        this.datePipe.transform(order.orderDate).toLowerCase().includes(term) ||
        order.sellerUserName.toString().toLowerCase().includes(term) ||
        order.buyerUserName.toString().toLowerCase().includes(term) ||
        this.decimalPipe.transform(order.orderAmount).includes(term) ||
        order.shippingStatus.toString().toLowerCase().includes(term)
      );
    });
  };
}
