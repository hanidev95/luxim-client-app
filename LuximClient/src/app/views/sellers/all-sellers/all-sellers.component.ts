import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SellerDto } from '../../../dtos/seller.dto';
import { DataService } from '../../../services/seller.service';

@Component({
  selector: 'app-all-sellers',
  templateUrl: './all-sellers.component.html',
  styleUrls: ['./all-sellers.component.scss'],
})
export class AllSellersComponent implements OnInit {
  sellers: SellerDto[];
  dtOptions: DataTables.Settings = {};

  constructor(
    private dataService: DataService,
    private toast: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.dtOptions = {
      paging: true,
      pageLength: 20,
      processing: true,
      lengthChange: false,
    };
    this.dataService.getSellers().subscribe(
      (response) => {
        this.sellers = response.data;
      },
      (err) => {
        this.toast.error('Something went wrong', 'Seller Orders');
      }
    );
  }

  redirectToSellerOrders = (sellerId: string) => {
    this.router.navigate(['seller-orders', sellerId]);
  };
}
