import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {
  SellerDetailDto,
  SellerOrderDetailDto,
} from '../../../dtos/seller.dto';
import { DataService } from '../../../services/seller.service';
import { orderStatus } from '../../../shared/enums/order-status.enum';

@Component({
  selector: 'app-seller-orders',
  templateUrl: './seller-orders.component.html',
  styleUrls: ['./seller-orders.component.scss'],
})
export class SellerOrdersComponent implements OnInit {
  id: string;
  sellerDetails: SellerDetailDto;
  sellerOrders: SellerOrderDetailDto[];
  dtOptions: DataTables.Settings = {};

  constructor(
    private dataService: DataService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
    this.dtOptions = {
      paging: true,
      pageLength: 20,
      processing: true,
      lengthChange: false,
    };
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });
  }

  ngOnInit(): void {
    this.getSellerDetails();
  }

  getSellerDetails = () => {
    this.dataService.getSellerDetails(this.id).subscribe(
      (response) => {
        this.sellerDetails = response.data;
        this.sellerOrders = response.data.orders;
      },
      (err) => {
        this.toastr.error(
          'No order found against this seller.',
          'Seller Orders'
        );
      }
    );
  };

  shouldTrackShipmentBeEnabled = (order: SellerOrderDetailDto) => {
    return !(
      order.orderStatus === orderStatus.Confirmed ||
      order.orderStatus === orderStatus.Shipped
    );
  };

  shouldPayoutBeEnabled = (order: SellerOrderDetailDto) => {
    return (
      order.orderStatus === orderStatus.Shipped && order.payoutStatus === 0
    );
  };

  trackShipment = (trackingId: string) => {
    if (!trackingId) {
      return;
    }

    this.dataService.trackShipmentStatus(trackingId).subscribe(
      (response) => {
        if (response.message) {
          this.toastr.toastrConfig.timeOut = 0;
          this.toastr.success(response.message, 'Shipment Status');
        }
      },
      (err) => {
        this.toastr.error(
          'No Tracking details found against this trackingId.',
          'Seller Orders'
        );
      }
    );
  };

  payoutToSeller = (orderId: number) => {
    this.dataService.payoutToSellerByOrder(orderId).subscribe(
      (response) => {
        if (response.data) {
          this.toastr.success(
            'Payout has been successful against this order.',
            'Payout'
          );
          this.getSellerDetails();
        } else {
          this.toastr.error('Order payout failed.', 'Payout');
        }
      },

      (err) => {
        this.toastr.error('Order payout failed', 'Seller Orders');
      }
    );
  };
}
