import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  formModel = {
    Email: '',
    Password: '',
  }
  loginError: string;
  constructor(private service: UserService, private router: Router) { this.loginError = ""; }

  ngOnInit(): void {

    if (localStorage.getItem('token')) {
      this.router.navigateByUrl('dashboard');
    }

  }

  onSubmit(form: NgForm) {

    //call login function from server
    this.service.login(form.value).subscribe(
      (response: any) => {
        console.log(response);
        localStorage.setItem("token", response.data.jwToken);
        localStorage.setItem("externalApiToken", response.data.externalApiAccessToken);
        //Redirect to Dashboads
        this.router.navigateByUrl('dashboard');

      }, err => {

        if (!err.error.Succeeded)
          this.loginError = err.error.Message;
        else
          console.log(err);

      })
  }

}
