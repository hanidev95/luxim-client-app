export interface IResponse {
  succeeded: boolean;
  message: string;
  statusCode: number;
  data: any;
}

export interface INewResponse<T> {
  succeeded: boolean;
  message: string;
  statusCode: number;
  data: T;
}
