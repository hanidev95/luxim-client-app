import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../environments/environment'
import { from } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  readonly BaseURL ='https://localhost:44353/api/';

  login(formData: any) {
    return this.http.post(environment.baseURL + 'Token', formData);
  }
}
