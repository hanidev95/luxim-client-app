import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import {
  IconModule,
  IconSetModule,
  IconSetService,
} from '@coreui/icons-angular';
import { AppComponent } from './app.component';
// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { UserService } from './shared/user.service';
import { AuthInterceptor } from './auth/auth.interceptor';

const APP_CONTAINERS = [DefaultLayoutComponent];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsModule } from './views/products/products.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CategoriesModule } from './views/categories/categories.module';
import { ReportedUsersComponent } from './views/reported-users/reported-users.component';
import { ReportedItemsComponent } from './views/reported-items/reported-items.component';
import { TestComponent } from './views/test/test.component';
import { ProductService } from './services/product.service';
import { JwPaginationModule } from 'jw-angular-pagination';
import { ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';
import { SellerOrdersComponent } from './views/sellers/seller-orders/seller-orders.component';
import { AllSellersComponent } from './views/sellers/all-sellers/all-sellers.component';
import { OrderHistoryComponent } from './views/pages/order-history/order-history.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    IconModule,
    IconSetModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ProductsModule,
    CategoriesModule,
    FormsModule,
    ReactiveFormsModule,
    JwPaginationModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({ timeOut: 2000, enableHtml: true }),
    DataTablesModule,
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    ReportedUsersComponent,
    ReportedItemsComponent,
    TestComponent,
    AllSellersComponent,
    SellerOrdersComponent,
    OrderHistoryComponent,
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    IconSetService,
    BsModalService,
    ProductService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
