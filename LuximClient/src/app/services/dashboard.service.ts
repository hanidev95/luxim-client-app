import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { IResponse } from '../shared/IResponseInterface';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  
  
  constructor(private httpClient: HttpClient) { }
  readonly BaseURL = environment.baseURL;

  getWidgetStats() {
    return this.httpClient.get<IResponse>(this.BaseURL + 'Dashboard/WidgetStats');
  }

  getReportedUsers() {

    return this.httpClient.get<IResponse>(environment.baseURL + 'Dashboard/ReportedUsers');
  }

  manageReportedUsers(form: any) {

    return this.httpClient.post<IResponse>(environment.baseURL + 'Dashboard/ManageReportedUser', form);
  }

  getorderHistory() {

    return this.httpClient.get<IResponse>(environment.baseURL + 'Dashboard/OrderHistory');
  }
}
