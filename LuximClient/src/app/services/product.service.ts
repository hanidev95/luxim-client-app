import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { IResponse } from '../shared/IResponseInterface';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class ProductService {
  // Members
  private paramSource = new BehaviorSubject(null);
  sharedParam = this.paramSource.asObservable();
  constructor(private httpClient: HttpClient) {}

  getAllProducts() {
    return this.httpClient.get<IResponse>(
      environment.baseURL + 'product/getall'
    );
  }

  manageProductApproval(form: any) {
    return this.httpClient.post<IResponse>(
      `${environment.baseURL}v1/product/approval-status`,
      form
    );
  }

  getReportedProducts() {
    return this.httpClient.get<IResponse>(
      environment.baseURL + 'product/reportedProducts'
    );
  }

  manageReportedProducts(form: any) {
    return this.httpClient.post<IResponse>(
      environment.baseURL + 'product/manageReportedProducts',
      form
    );
  }

  getproductImages(productId: number) {
    return this.httpClient.get<IResponse>(
      environment.baseURL + 'product/Images/' + productId
    );
  }

  getproductRejectionReasons() {
    return this.httpClient.get<IResponse>(
      environment.baseURL + 'category/RejectionReasons'
    );
  }
}
