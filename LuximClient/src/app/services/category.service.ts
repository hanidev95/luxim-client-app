import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResponse } from '../shared/IResponseInterface';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient: HttpClient) { }

  getAllCategories() {
    return this.httpClient.get<IResponse>(environment.baseURL + "category")
  }


  getAllSubCategories() {
    return this.httpClient.get<IResponse>(environment.baseURL + "category/subCategory")
  }

  getSubCategoryById(id: number) {
    return this.httpClient.get<IResponse>(environment.baseURL + "category/SubCategory/" + id);
  }

  manageSubCategories(form: any) {

    return this.httpClient.post<IResponse>(environment.baseURL + "category/subCategory", form)
  }

  checkIfSubCategoryExists(request: any) {
    return this.httpClient.get<IResponse>(`${environment.baseURL}category/SubCategory/exists?name=${request.subCategoryTitle}&categoryId=${request.categoryId}&subCategoryReference=${request.subCategoryReference}`);
  }

}
