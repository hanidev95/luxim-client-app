import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { DashboardWidgetDto } from '../dtos/dashboard.dto';
import { OrderHistoryDto } from '../dtos/orders.dto';
import { SellerDto, SellerDetailDto } from '../dtos/seller.dto';
import { INewResponse } from '../shared/IResponseInterface';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private httpClient: HttpClient) {}

  getSellers() {
    return this.httpClient.get<INewResponse<SellerDto[]>>(
      environment.baseURL + 'v1/seller'
    );
  }

  getSellerDetails(sellerId: string) {
    return this.httpClient.get<INewResponse<SellerDetailDto>>(
      `${environment.baseURL}v1/seller/${sellerId}/details`
    );
  }

  payoutToSellerByOrder(orderId: number) {
    return this.httpClient.get<INewResponse<boolean>>(
      `${environment.baseURL}v1/seller/${orderId}/payout`
    );
  }

  trackShipmentStatus(trackingId: string) {
    return this.httpClient.get<INewResponse<string>>(
      `${environment.baseURL}v1/seller/track-shipment/${trackingId}`
    );
  }

  getAllOrders() {
    return this.httpClient.get<INewResponse<OrderHistoryDto[]>>(
      `${environment.baseURL}v1/order`
    );
  }

  getWidgetData() {
    return this.httpClient.get<INewResponse<DashboardWidgetDto>>(
      `${environment.baseURL}v1/dashboard`
    );
  }
}
