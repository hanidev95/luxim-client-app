export interface reportedUserModel {
  createdBy: string,
  reportedBy: string,
  reportedUser: string,
  reason: string,
  userId: string,
  created: string,
  status: boolean
}
