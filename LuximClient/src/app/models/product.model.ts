export interface productmodel {
  id: number,
  title: string,
  description: string,
  price: number,
  approvalStatus: string,
  isActive: boolean,
  categoryId: number,
  category: string,
  subCategory: string,
  color: string,
  addedBy: string,
  created: string,
  rejectionReason: string,
  productImages :  [string],
  rejectionReasonId : number 
}


export interface reportedProductModel {
  productId: number,
  createdBy: string,
  reportedBy: string,
  productTitle: string,
  category: string,
  reason: string,
  created: string,
  status : boolean
}


export interface productImagesModel {
  productId : number,
  imagePath : string
}

//Enum

export interface orderHistoryModel {
    orderId: number,
    orderNumber:  string ,
    productTitle :  string ,
    description :  string ,
    orderItemAmount : 0,
    orderDate :  string ,
    quantity : number,
    itemStatus : string,
    sellerName :  string ,
    buyerName :  string ,
    trackingId :  string ,
    productId : number,
    size :  string ,
    sellerId :  string ,
    productImages :  string 
}

//product rejection reasons

export interface productRejectionReasons {

 reasonId : number,
 name  : string,
 description : string

}

