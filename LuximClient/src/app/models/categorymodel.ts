export interface subCategorymodel {
  id: number,
  category: string,
  name: string,
  description: string,
  subCategoryReference: string,
  categoryId: number,
  isActive: boolean
}

export interface categoryModel {
  id: number,
  name: string,
  description: string,
  createdBy: string,
  created: string,
  isActive: boolean
}
