export interface sellerOrderInfo {
    sellerId: string,
    seller: string,
    totalAmount : number,
    receivedAmount: number,
    pendingAmount : number,
    luximCommission : number,
    luximProcessingFee : number
  }


  export interface sellerOrderDetailInfo {
    orderId : BigInteger,
    wayBillNumber: string,
    orderNumber: string,
    orderAmount : number,
    chipmentStatus: number,
    createdDate : number,
    paymentStatus : string,
    payoutEnabled : boolean,
    seller : string,
    sellerEmail : string,
    bankName : string,
    nameOnAccount : string,
    accountNumber : string,
    luximCommission : number
    amountToBePaid : number,
    luximProcessingFee : number
  }