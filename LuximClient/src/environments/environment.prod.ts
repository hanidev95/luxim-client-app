export const environment = {
  production: true,
  baseURL: 'http://luxim-admin-prod.us-east-2.elasticbeanstalk.com/api/',
};
